int analogSensorPin = A0;
int humidityValue = 0;
int sensorDry = 850;
int sensorWet = 640;

void setup () {
  Serial.begin(9600); // setup serial
}

void loop () {
  humidityValue = analogRead(analogSensorPin);
  int humidityValueCalibrated = map(humidityValue, sensorWet, sensorDry, 100, 0);
  if (humidityValueCalibrated > 100) {
    Serial.println(100);
  }
  else if (humidityValueCalibrated < 0) {
    Serial.println(0);
  }
  else {
    Serial.println(humidityValueCalibrated);
  }
  delay(1000);
}