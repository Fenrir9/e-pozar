int analogSensorPin = A0;
int analodRead = 0;

void setup() {
  Serial.begin(9600);
}
void loop() {
  analodRead = analogRead(analogSensorPin);
  int mappedValue = map(analodRead, 0, 1023, 0, 100);

  if (mappedValue > 100) {
    Serial.println(100);
  }
  else if (mappedValue < 0) {
    Serial.println(0);
  }
  else {
    Serial.println(mappedValue);
  }
  delay(1000);
}