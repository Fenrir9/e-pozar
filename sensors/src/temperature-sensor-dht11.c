#include <dht11.h>
dht11 DHT11;
#define DHT11PIN 2

void setup(){
  Serial.begin(9600);   //serial setup
}
void loop(){
  int chk = DHT11.read(DHT11PIN);
  Serial.print("Air humidity [%]: ");
  Serial.print(DHT11.humidity, 1);
  Serial.print(",\t");
  Serial.print("Temperature [C]: ");
  Serial.println(DHT11.temperature, 1);
  delay(1000);
}