#include "dht.h"
dht DHT22;
#define DHT22PIN 2

void setup(){
  Serial.begin(9600);
}

void loop(){
  int chk = DHT22.read(DHT22PIN);
  Serial.print("Air humidity (%): ");
  Serial.print(DHT22.humidity, 1);
  Serial.print(",\t");
  Serial.print("Temperature (C): ");
  Serial.println(DHT22.temperature, 1);
  delay(1000);
}