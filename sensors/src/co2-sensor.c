#include <SoftwareSerial.h>
SoftwareSerial mySerial(10, 11);
unsigned char hexdata[9] = {0xFF, 0x01, 0x86, 0x00, 0x00, 0x00, 0x00, 0x00, 0x79};

void setup() {
  Serial.begin(9600);
  mySerial.begin(9600);
}

void loop() {
  mySerial.write(hexdata, 9);
  delay(500);

  for (int i = 0, j = 0; i < 9; i++) {
    if (mySerial.available() > 0) {
      long hi, lo, CO2;
      int ch = mySerial.read();

      if (i == 2) {
        hi = ch;
      }
      if (i == 3) {
        lo = ch;
      }
      if (i == 8) {
        CO2 = hi * 256 + lo;
        Serial.print(CO2);
        Serial.println(" ppm");
      }
    }
  }
}