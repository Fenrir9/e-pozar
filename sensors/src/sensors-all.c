#include <SoftwareSerial.h>
#include "dht.h"
#define DHT22PIN 2
#define SOILPIN A0
#define SMOKEPIN A1
#define ONEMIN 60000UL
#define ONEHOUR 3600000UL

dht DHT22;
SoftwareSerial mySerial(10, 11);
unsigned char hexdata[9] = {0xFF, 0x01, 0x86, 0x00, 0x00, 0x00, 0x00, 0x00, 0x79};

int sensorValues[5]; //[temp, air humidity, soil humidity, smoke, co2]


void setup() {
  Serial.begin(9600);
  mySerial.begin(9600);
}

void loop() {
  int soilHumidityRead = analogRead(SOILPIN);
  int smokeRead = analogRead(SMOKEPIN);
  int soilHumidityValueMapped = map(soilHumidityRead, 640, 850, 100, 0); // sensor wet, sensor dry
  int smokeValueMapped = map(smokeRead, 0, 1023, 0, 100);

  // temp, air humidity
  int chk = DHT22.read(DHT22PIN);
  sensorValues[0] = (int)DHT22.temperature;
  sensorValues[1] = (int)DHT22.humidity;

  // soil humidity
  if (soilHumidityValueMapped > 100) {
    sensorValues[2] = 100;
  }
  else if (soilHumidityValueMapped < 0) {
    sensorValues[2] = 0;
  }
  else {
    sensorValues[2] = soilHumidityValueMapped;
  }

  // smoke
  if (smokeValueMapped > 100) {
    sensorValues[3] = 100;
  }
  else if (smokeValueMapped < 0) {
    sensorValues[3] = 0;
  }
  else {
    sensorValues[3] = smokeValueMapped;
  }

  // co2
  mySerial.write(hexdata, 9);
  delay(500);

  for (int i = 0, j = 0; i < 9; i++) {
    if (mySerial.available() > 0) {
      long hi, lo, CO2;
      int ch = mySerial.read();

      if (i == 2) {
        hi = ch;
      }
      if (i == 3) {
        lo = ch;
      }
      if (i == 8) {
        CO2 = hi * 256 + lo;
        sensorValues[4] = CO2;
      }
    }
  }


  Serial.println("Odczyt: [temp, air humidity, soil humidity, smoke, co2]");

  for (int i = 0; i < 5; i++) {
    Serial.print(sensorValues[i]);
    Serial.print(", ");
  }
  Serial.println();

  delay(ONEMIN); // delay time between sensor reads
}