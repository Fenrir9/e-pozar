# Sensors Documentation

### Pojemnościowy czujnik wilgotności gleby
https://abc-rc.pl/product-pol-12417-Czujnik-wilgotnosci-gleby-M335-odporny-na-korozje-Arduino.html

Czujnik wilgotności gleby M335 wykorzystuje wykrywanie pojemnościowe wilgoci w glebie.
Wykonany jest z materiałów odpornych na korozję co znacznie wydłuża jego żywotność. Głównym modułem jest popularny NE555.

<img src="./images/soil-humidity-sensor.png"  width="40%" >


#### Dane techniczne
**Napięcie robocze:** 3,3 - 6 VDC \
**Napięcie wyjściowe:** 0 ~ 3,0 VDC\
**Interfejs:** PH2.54-3P \
**Układ scalony:** NE555

<img src="images/soil-humidity-sensor-schematic.png"  width="60%" >
<img src="images/soil-humidity-sensor-output.png"  width="10%" >


Czujnik został skalibrowany w taki sposób, aby wyświetlał wartości wilgotnosci gleby w procentach. 

### Czujnik dymu i łatwopalnych gazów MQ-2 - półprzewodnikowy
https://botland.com.pl/czujniki-gazow/1911-czujnik-dymu-i-latwopalnych-gazow-mq-2-polprzewodnikowy-modul-czarny-5904422302238.html \
http://www.cmr.losto.net/l12.php \
Datasheet: http://www.cmr.losto.net/l12.php 

Konstrukcja czujnika bazuje na dwutlenku krzemu (SnO2), którego rezystancja gwałtownie 
spada po wykryciu gazów bądź dymu. Obok samego czujnika MQ-2 na płytce znalazł się także prosty układ komparatora, 
udostępniającego dodatkowe, cyfrowe (binarne) wyjście o regulowanym progu przełączania.

Czujnik wykrywa stężenie palnych gazów w powietrzu lub dym a wynik można uzyskać z pomiaru napięcia na wyjściu analogowym, które podłączamy bezpośrednio do Arduino.
Nadaje się do systemów wykrywania wycieków gazów oraz dymu.

<img src="images/smoke-sensor.png"  width="30%" >


#### Dane techniczne
**Zasilanie:** 5 V \
**Pobór prądu:** 150 mA \
**Zakres pomiarowy** do 10 000 ppm  \
**Temperatura pracy:** od -20 do 50 °C \

#### Zalety
- wytrzymały i tani
- dość wysoka czułość

<img src="images/smoke-sensor-schematic.png"  width="40%" >
<img src="images/smoke-sensor-output.png"  width="10%" >


### DFrobot Gravity - czujnik dwutlenku węgla CO2 IR 0-50000ppm - UART
https://botland.com.pl/gravity-czujniki-gazow-i-pylow/8110-dfrobot-gravity-czujnik-dwutlenku-wegla-co2-ir-0-50000ppm-uart.html \
https://github.com/Arduinolibrary/DFRobot_Gravity_UART_Infrared_CO2_Sensor/raw/master/MH-Z16%20CO2%20Datasheet.pdf \
Docs: https://wiki.dfrobot.com/Infrared_CO2_Sensor_0-50000ppm_SKU__SEN0220 

Wysokiej precyzji czujnik wykrywający stężenie CO2 (dwutlenku węgla) w powietrzu. 
Wyposażony jest w interfejs UART, pracuje z napięciem od 4,5 V do 5,5 V. Działa w zakresie od 0 do 50000 ppm.
Moduł cechuje się wysoką czułością, wysoką rozdzielczością, niskim poborem energii, krótkim czasem reakcji, wysoką stabilnością oraz długą żywotnością. 
Nie wpływa na niego para wodna oraz nie jest toksyczny. 
Czujnik znajduje zastosowanie w pomieszczeniach chłodniczych, wewnętrznych systemach jakości powietrza, w procesach monitoringu hodowli zwierząt i produkcji rolnej, itp.

Czujnik powinien być kalibrowany regularnie -- co nie więcej niż 6 mięsięcy

#### Dane techniczne
**Napięcie zasilania:** od 4,5 V do 5,5 V \
**Pobierany prąd:** ok. 85 mA  \
**Poziom sygnałów logicznych:** 3,3 V / 5 V \
**Sygnał wyjściowy:** UART \
**Zakres pomiarowy:** od 0 do 50000 ppm \
**Dokładność:**  +/- 50 ppm + 5 % odczyt \
**Czas podgrzewania:** 3 min \
**Czas odpowiedzi:** 120 s  \
**Temperatura pracy:** od 0 °C do 50 °C \
**Wilgotność pracy:** od 0 % do 95 % RH  \
**Żywotność:** ponad 5 lat  

<img src="images/co2-sensor-schematic.png"  width="70%" >
<img src="images/co2-sensor-output.png"  width="15%" >

### Czujnik temperatury i wilgotności DHT11
https://botland.com.pl/czujniki-multifunkcyjne/1886-czujnik-temperatury-i-wilgotnosci-dht11-modul-przewody.html \
https://botland.com.pl/content/140-arduino-i-obsluga-czujnika-temperatury-i-wilgotnosci-dht11 \
Biblioteka do Arduino: https://playground.arduino.cc/Main/DHT11Lib/ \
Dokumentacja: https://botland.com.pl/index.php?controller=attachment&id_attachment=251

Popularny czujnik temperatury i wilgotności powietrza z interfejsem cyfrowym, jednoprzewodowym.
Składa się on się z małej płytki PCB – zawierającej rezystancyjny czujnik wilgotności, termistor i 8-bitowy mikrokontroler – umieszczonej w tworzywowej, perforowanej obudowie. 
Sensor posiada cztery wyprowadzenia, z których dwa stanowią podłączenie zasilania (3,3 V do 5 V), zaś trzecie służy do cyfrowej, 
dwukierunkowej komunikacji z mikrokontrolerem (czwarte wyprowadzenie jest niepodłączone). 
Czujnik nie wymaga do poprawnej pracy żadnych zewnętrznych elementów peryferyjnych, 

Układ zawiera wszystkie, niezbędne do poprawnego działania czujnika komponenty pasywne. Moduł wystarczy podłączyć za pomocą przewodów z dowolnym mikrokontrolerem lub zestawem uruchomieniowym:

VCC ( + ) - napięcie zasilania z zakresu: 3 V do 5,5 V \
GND ( - ) - masa układu \
OUT - wyjście cyfrowe podłączane do wejścia mikrokontrolera

<img src="images/temperature-sensor-dht11.png"  width="40%" >


#### Dane techniczne
**Napięcie zasilania:** 3,3 V do 5,5 V \
**Średni pobór prądu:** 0,2 mA

##### Temperatura
**Zakres pomiarowy:** 0 - 50 °C \
**Rozdzielczość:** 8-bitów (1 °C) \
**Dokładność:** 1 °C  \
**Czas odpowiedzi:** 6 - 15 s (typowo 10 s)

##### Wilgotność
**Zakres pomiarowy:** od 20 % do 90 % RH \
**Rozdzielczość:** 8-bitów (±1 % RH*) \
**Dokładność**  ±4 RH* (przy 25 °C) \
**Zakres pomiarowy:** 6 - 30 s 

<img src="images/temperature-sensor-dht11-schematic.jpeg"  width="50%" >
<img src="images/temperature-sensor-dht11-output.png"  width="50%" >

#### Użycie
Najpierw należy pobrać bibliotekę czujnika `dht11`,a następnie dodać jądo środowiska Arduino (Szkic -> Include Library -> Add .ZIP Library...). 


### Czujnik temperatury i wilgotności DHT22

https://botland.com.pl/czujniki-multifunkcyjne/2637-czujnik-temperatury-i-wilgotnosci-dht22-am2302-modul-przewody.html \
Dokumentacja: https://botland.com.pl/index.php?controller=attachment&id_attachment=337 \
Biblioteka Arduino: http://playground.arduino.cc/Main/DHTLib

VCC ( + ) - napięcie zasilania z zakresu: 3 V do 5,5 V \
GND ( - ) - masa układu \
OUT - wyjście cyfrowe podłączane do wejścia mikrokontrolera

<img src="images/temperature-sensor-dht22.png"  width="40%" >


#### Dane techniczne
**Napięcie zasilania:** od 3,3 V do 5,5 V \
**Średni pobór prądu:** 0,2 mA

##### Temperatura
Zakres pomiarowy: -40 do +80 °C \
Rozdzielczość: 8-bitów (0,1 °C) \
Dokładność: ± 0,5 °C \
Czas odpowiedzi: średnio 2 s

##### Wilgotność
**Zakres pomiarowy:** 0 - 100 % RH \
**Rozdzielczość:** 8-bitów (±0,1 % RH) \
**Dokładność:** ±2 %RH* \
**Czas odpowiedzi:** średnio 2 s

<img src="images/temperature-sensor-dht22-schematic.png"  width="50%" >
<img src="images/temperature-sensor-dht22-output.png"  width="50%" >
