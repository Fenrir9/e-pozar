import socket
import time
import utime
import binascii
import ubinascii
import pycom
import ustruct
import struct
import machine
import dht
import machine

from network import LoRa
from machine import Pin
from machine import UART

LORA_FREQUENCY = 868100000
LORA_NODE_DR = 5

COLOUR_WHITE = 0xFFFFFF
COLOUR_BLACK = 0x000000
COLOUR_RED   = 0xFF0000
COLOUR_GREEN = 0x00FF00
COLOUR_BLUE  = 0x0000FF
COLOUR_ORANGE = 0xFFa500


def remap(value, leftMin, leftMax, rightMin, rightMax):
    # remapping to find proper values
    leftSpan = leftMax - leftMin
    rightSpan = rightMax - rightMin
    # convert the left range into a 0-1 range (float)
    valueScaled = float(value - leftMin) / float(leftSpan)
    # convert the 0-1 range into a value in the right range.
    # return int(rightMin + (valueScaled * rightSpan))
    return rightMin + (valueScaled * rightSpan)


def soil_measure():

    adc1 = machine.ADC() # soil
    apin1 = adc1.channel(pin = 'P13', attn = machine.ADC.ATTN_11DB)

    value = apin1()
    print("SOIL MEASURE: ADC count = %d" %(value))

    # calibration
    # min(dry) and max(wet) ADC values
    max_ADC_value = 3150
    min_ADC_value = 1500

    # prevent values above range (and percentages over 100% / above 0%)
    if value < min_ADC_value:
        value = min_ADC_value
    elif value > max_ADC_value:
        value = max_ADC_value
    else:
        value = value

    moist_percent = remap(value, min_ADC_value, max_ADC_value, 100, 1)

    return moist_percent


def temp_measure():

    th = dht.DTH(Pin('P9', mode=Pin.OPEN_DRAIN),1)

    ### temperature ###
    result = th.read()
    temperature = result.temperature/1.0

    if temperature == 0.0:
        print('Measuring again...')
        result = th.read()

    else:
        temperature = result.temperature/1.0
        pass

    return temperature


def air_humidity_measure():

    th = dht.DTH(Pin('P9', mode=Pin.OPEN_DRAIN),1)

    ### air_humidity ###
    result = th.read()
    air_humidity = int(result.humidity/1.0)

    return air_humidity


def smoke_measure():

    adc = machine.ADC() # smoke
    apin = adc.channel(pin = 'P15')

    value = apin()
    print("SMOKE MEASURE: ADC count = %d" %(value))

    smoke_value = int((value * 100) / 1023)

    return smoke_value


def co2_measure():

    hexdata = (0xFF,0x01,0x86,0x00,0x00,0x00,0x00,0x00,0x79)
    hexdata = bytes(hexdata)
    uart.write(hexdata)
    time.sleep(0.2)
    hi = uart.read()
    co2 = hi[2]*256+hi[3]

    return co2


lora = LoRa(mode=LoRa.LORAWAN, region=LoRa.EU868)

# create an OTAA authentication parameters, change them to the provided credentials
app_eui = ubinascii.unhexlify('0000000000000000')
app_key = ubinascii.unhexlify('A46087AFCA9306C980514FD40B432C96')
dev_eui = ubinascii.unhexlify('70b3d5499aa367e1')

for i in range(0, 15):
    lora.remove_channel(i)

lora.add_channel(0, frequency=LORA_FREQUENCY, dr_min=0, dr_max=5)
lora.add_channel(1, frequency=LORA_FREQUENCY, dr_min=0, dr_max=5)
lora.add_channel(2, frequency=LORA_FREQUENCY, dr_min=0, dr_max=5)

lora.join(activation=LoRa.OTAA, auth=(app_eui, app_key), timeout=0)
# lora.join(activation=LoRa.OTAA, auth=(dev_eui, app_eui, app_key), timeout=0)

# wait for a connection
print('Waiting for LoRaWAN network connection...')
while not lora.has_joined():
	time.sleep(1)


# we're online, set LED to green and notify via print
# pycom.rgbled(COLOUR_GREEN)
print('Network joined!')

# setup the socket
s = socket.socket(socket.AF_LORA, socket.SOCK_RAW)
s.setsockopt(socket.SOL_LORA, socket.SO_DR, 5)
s.setblocking(False)
s.bind(1)

uart = UART(1, baudrate=9600, pins=('P3','P4'))
##### TODO - TTN decoder
while True:

    time.sleep(1)
    air_humidity = air_humidity_measure()
    temperature = temp_measure()
    smoke_value = smoke_measure()
    soil_moist_percent = soil_measure()
    co2 = co2_measure()

    print(" Air humidity = ", float(air_humidity), "\n",
            "Temperature = ", temperature, "\n",
            "Smoke = ", smoke_value, "\n",
            "Soil humidity = ", soil_moist_percent, "\n",
            "CO2 = ", co2, "\n")

	# encode the packet, so that it's in BYTES (TTN friendly)
	# could be extended like this struct.pack('f', temp) + struct.pack('c',"example text")
    # 'h' packs it into a short, 'f' packs it into a float, must be decoded in TTN

    packet = ustruct.pack('f', soil_moist_percent) + ustruct.pack('f', temperature) + ustruct.pack('f', air_humidity) + ustruct.pack('f', smoke_value) + ustruct.pack('f', co2)
	# send the prepared packet via LoRa
    s.send(packet)

	# check for a downlink payload, up to 64 bytes
    rx_pkt = s.recv(64)

	# check if a downlink was received
    if len(rx_pkt) > 0:
		print("Downlink data on port 200:", rx_pkt)
		pycom.rgbled(COLOUR_ORANGE)
		input("Downlink recieved, press Enter to continue")
		pycom.rgbled(COLOUR_GREEN)

    time.sleep(10)
