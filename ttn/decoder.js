
function Decoder(bytes, port) {

    // Based on https://stackoverflow.com/a/37471538 by Ilya Bursov
    function bytesToFloat(bytes) {
      // JavaScript bitwise operators yield a 32 bits integer, not a float.
      // Assume LSB (least significant byte first).
      var bits = bytes[3]<<24 | bytes[2]<<16 | bytes[1]<<8 | bytes[0];
      var sign = (bits>>>31 === 0) ? 1.0 : -1.0;
      var e = bits>>>23 & 0xff;
      var m = (e === 0) ? (bits & 0x7fffff)<<1 : (bits & 0x7fffff) | 0x800000;
      var f = sign * m * Math.pow(2, e - 150);
      return f;
    }  
    var wilgotonosc_gleby = bytesToFloat(bytes.slice(0, 4))
    var dym = bytesToFloat(bytes.slice(12, 16))
    var co_2 = bytesToFloat(bytes.slice(16, 20))
    var _temperatura = bytesToFloat(bytes.slice(4, 8))
    var powietrze = bytesToFloat(bytes.slice(8, 12))
    
    
    // Test with 0082d241 for 26.3134765625
    return {
      // Take bytes 0 to 4 (not including), and convert to float:
      temperatura: _temperatura, wilgotnosc_gleby: wilgotonosc_gleby, dwutlenek: co_2, zadymienie: dym, wilotnosc_powietrza: powietrze 
    };
  }